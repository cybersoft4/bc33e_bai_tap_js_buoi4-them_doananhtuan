// B1:Viết chương trình nhập vào ngày, tháng, năm (Giả sử nhập đúng, không cần kiểm tra hợp lệ).
/*
input: ngày, tháng, năm 
action:
dùng 3 biến để nhận 3 giá trị ngày tháng năm từ người dùng, sau đó xét các trường hợp cuối tháng và đầu tháng, cuối 
năm, năm nhuận, năm thường.....
output: ngày, tháng, năm của ngày tiếp theo và trước đó
*/
function preDay(){
    const date=document.getElementById("day").value*1
    const month=document.getElementById("month").value*1
    const year=document.getElementById("year").value*1
    const preDay=document.getElementById("preDay")
    const nextDay=document.getElementById("nextDay")
    // xuất ra ngày trước 
    if(date==1){
        if(month==3&&year%4==0){
            preDay.innerHTML=`29/${month-1}/${year}`
        }else if(month==3&&year%4==1||month==3&&year%4==2||month==3&&year%4==3){
            preDay.innerHTML=`28/${month-1}/${year}`
        }else if(month==2||month==4||month==6||month==9||month==11||month==8){
            preDay.innerHTML=`31/${month-1}/${year}`
        }else if(month==3||month==5||month==7||month==10||month==12){
            preDay.innerHTML=`30/${month-1}/${year}`
        }else if(month==1){
            preDay.innerHTML=`31/12/${year-1}`
        }
    }else{
        preDay.innerHTML=`${date-1}/${month}/${year}`
    }
    // xuất ra ngày sau
    if(date==30&&month==1||date==30&&month==4||date==30&&month==6||date==30&&month==9||date==30&&month==11){
        nextDay.innerHTML=`1/${month+1}/${year}`
    }else if(date==31){
        nextDay.innerHTML=`1/${month+1}/${year}`
    }else if(date==29&&month==2){
        nextDay.innerHTML=`1/3/${year}`
    }else if(date==28&&year%4==0){
        nextDay.innerHTML=`${date+1}/${month}/${year}`
    }else if(date==28&&year%4==1||date==28&&year%4==2||date==28&&year%4==3){
        nextDay.innerHTML=`1/3/${year}`
    }else{nextDay.innerHTML=`${date+1}/${month}/${year}`}
}

// B2:Viết chương trình nhập vào tháng, năm. Cho biết tháng đó có bao nhiêu ngày. (bao gồm tháng
// của năm nhuận)..
/*
input: tháng, năm 
action:
nếu là tháng 1-3-5-7-8-10-12: thì 31 ngày
nếu là tháng 4,6,9,11: thì 31 ngày
tháng 2 năm nhuận 29 ngày
tháng 2 năm thường  28 ngày
output: ngày của tháng, năm đã nhập
*/
function dayOfMonth(){
    const month1=document.getElementById("month1").value*1
    const year1=document.getElementById("year1").value*1
    const result=document.getElementById("result")
    if(month1==1||month1==3||month1==5||month1==7||month1==8||month1==10||month1==12){
        result.innerHTML="31 ngày"
    }else if(month1==4||month1==6||month1==9||month1==11){
        result.innerHTML="30 ngày"
    }else if(month1==2&year1%4==0){
        result.innerHTML="29 ngày"
    }else if(month1==2&year1%4==1||month1==2&year1%4==2||month1==2&year1%4==3){
        result.innerHTML="28 ngày"
    }else{result.innerHTML="Xin vui lòng nhập lại"}
}
// B3:Viết chương trình nhập vào số nguyên có 3 chữ số. In ra cách đọc nó.
/*
input: số có 3 chữ số 
action:
chia số 3 thành chữ số riêng biệt (trăm, chục, đơn vị)
sau đó xét từng trường hợp nếu bằng 1 thì in ra "một.."
...
output: cách đọc
*/
function docso(){
    const tram=document.getElementById("tram")
    const chuc=document.getElementById("chuc")
    const donvi=document.getElementById("donvi")
    const number=document.getElementById("number").value*1
    if(99<number&number<1000&Number.isInteger(number)){
        var a=Math.floor(number/100)
        console.log(a)
        var b=Math.floor((number / 10) % 10)
        var c=number%10
        switch(a){
            case 1:{tram.innerHTML="một trăm" ;break}
            case 2:{tram.innerHTML="hai trăm";break}
            case 3:{tram.innerHTML="ba trăm";break}
            case 4:{tram.innerHTML="bốn trăm";break}
            case 5:{tram.innerHTML="năm trăm";break}
            case 6:{tram.innerHTML="sáu trăm";break}
            case 7:{tram.innerHTML="bảy trăm";break}
            case 8:{tram.innerHTML="tám trăm";break}
            case 9:{tram.innerHTML="chín trăm";break}
        }
        switch(b){
            case 0:{chuc.innerHTML=`${c>0?"lẻ":""}` ;break}
            case 1:{chuc.innerHTML="mười" ;break}
            case 2:{chuc.innerHTML="hai mươi";break}
            case 3:{chuc.innerHTML="ba mươi";break}
            case 4:{chuc.innerHTML="bốn mươi";break}
            case 5:{chuc.innerHTML="năm mươi";break}
            case 6:{chuc.innerHTML="sáu mươi";break}
            case 7:{chuc.innerHTML="bảy mươi";break}
            case 8:{chuc.innerHTML="tám mươi";break}
            case 9:{chuc.innerHTML="chín mươi";break}
        }
        switch(c){
            case 0:{donvi.innerHTML="" ;break}
            case 1:{donvi.innerHTML="một" ;break}
            case 2:{donvi.innerHTML="hai";break}
            case 3:{donvi.innerHTML="ba";break}
            case 4:{donvi.innerHTML="bốn";break}
            case 5:{donvi.innerHTML="năm";break}
            case 6:{donvi.innerHTML="sáu";break}
            case 7:{donvi.innerHTML="bảy";break}
            case 8:{donvi.innerHTML="tám";break}
            case 9:{donvi.innerHTML="chín";break}
        }
    }else{tram.innerHTML="Nhập không đúng"}
}
// B4:Viết chương trình in tên sinh viên xa trường nhất.
/*
input: tọa độ trường và tọa độ của 3 sinh viên, tên của 3 sinh viên
action:
lấy giá trị của các tọa độ đầu vào sau đó tính khoảng cách bằng công thức KC*KC=(X1-X2)*(X1-X2)+(Y1-Y2)*(Y1-Y2)
sau đó so sánh khoảng cách giũa các sinh viên
output: tên của sinh viên xa trường nhất
*/
function toado(){
    const Xtruong=document.getElementById("Xtruong").value*1
    const Ytruong=document.getElementById("Ytruong").value*1
    const XA=document.getElementById("XA").value*1
    const YA=document.getElementById("YA").value*1
    const XB=document.getElementById("XB").value*1
    const YB=document.getElementById("YB").value*1
    const XC=document.getElementById("XC").value*1
    const YC=document.getElementById("YC").value*1
    const result10=document.getElementById("result10")
    const khoangcachA=Math.sqrt((Xtruong-XA)*(Xtruong-XA)+(Ytruong-YA)*(Ytruong-YA))
    const khoangcachB=Math.sqrt((Xtruong-XB)*(Xtruong-XB)+(Ytruong-YB)*(Ytruong-YB))
    const khoangcachC=Math.sqrt((Xtruong-XC)*(Xtruong-XC)+(Ytruong-YC)*(Ytruong-YC))
    if(khoangcachA>khoangcachB&&khoangcachA>khoangcachC){
        result10.innerHTML="Sinh viên A"
    }else if(khoangcachB>khoangcachA&&khoangcachB>khoangcachC){
        result10.innerHTML="Sinh viên B"
    }else if(khoangcachC>khoangcachA&&khoangcachC>khoangcachB){
        result10.innerHTML="Sinh viên C"
    }else return
}